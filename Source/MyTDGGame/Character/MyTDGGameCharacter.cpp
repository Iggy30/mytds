// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDGGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include"Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "Math/UnrealMathUtility.h"
#include"Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "MyTDGGame/Game/MyTDGGameInstance.h"



AMyTDGGameCharacter::AMyTDGGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UMyTDGGameInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UMyMyTDGGame_CharHealthComponent>(TEXT("HealthComponent"));
	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &AMyTDGGameCharacter::CharDead);
	}
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AMyTDGGameCharacter::InitWeapon);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AMyTDGGameCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void AMyTDGGameCharacter::BeginPlay()
{
	Super::BeginPlay();

	

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void AMyTDGGameCharacter::SetupPlayerInputComponent(UInputComponent* InputComponents)
{
	Super::SetupPlayerInputComponent(InputComponents);

	InputComponents->BindAxis(TEXT("MoveForward"), this, &AMyTDGGameCharacter::InputAxisX);
	InputComponents->BindAxis(TEXT("MoveRight"), this, &AMyTDGGameCharacter::InputAxisY);
	InputComponents->BindAction("MovementModeChangeSprint", IE_Pressed, this, &AMyTDGGameCharacter::Sprint);
	InputComponents->BindAction("MovementModeChangeSprint", IE_Released, this, &AMyTDGGameCharacter::Sprint);
	InputComponents->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this,
	                            &AMyTDGGameCharacter::InputAttackPressed);
	InputComponents->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this,
	                            &AMyTDGGameCharacter::InputAttackReleased);
	InputComponents->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this,
	                            &AMyTDGGameCharacter::TryReloadWeapon);
	InputComponents->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AMyTDGGameCharacter::TrySwicthNextWeapon);
	InputComponents->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &AMyTDGGameCharacter::TrySwitchPreviosWeapon);
	InputComponents->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &AMyTDGGameCharacter::TryAbilityEnabled);
}

void AMyTDGGameCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void AMyTDGGameCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}


void AMyTDGGameCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void AMyTDGGameCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AMyTDGGameCharacter::MovementTick(float DetlaTime)
{
	if (bIsAlive)
	{

		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);


		if (MovementState == EMovementState::SprintRun_State)
		{
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();
			SetActorRotation((FQuat(myRotator)));
		}
		else
		{
			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (myController)
			{
				FHitResult ResultHit;
				//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
				myController->GetHitResultUnderCursor(ECC_GameTraceChannel11, true, ResultHit);

				float FindRotatorResult = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).
					Yaw;
				SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResult, 0.0f)));

				if (CurrentWeapon)
				{
					FVector Displacement = FVector(0);
					switch (MovementState)
					{
					case EMovementState::Aim_State:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						CurrentWeapon->ShouldReduceDispersion = true;
						break;
					case EMovementState::AimWalk_State:
						CurrentWeapon->ShouldReduceDispersion = true;
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						break;
					case EMovementState::Walk_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = false;
						break;
					case EMovementState::Run_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = false;
						break;
					case EMovementState::SprintRun_State:
						break;
					default:
						break;
					}
					CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
					//aim cursor like 3D Widget?(���� �� ����� � ��� ������ ���)
				}
			}
		}
		Sprint();
	}
}

void AMyTDGGameCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Chheck Meele or Range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AMyTDGGameCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}

void AMyTDGGameCharacter::Sprint()
{
	if (MovementState == EMovementState::SprintRun_State)
	{
		AddMovementInput(GetActorForwardVector(), (GetCharacterMovement()->MaxWalkSpeed));


		float DotProduct = FVector::DotProduct(GetActorForwardVector(), GetVelocity() * 0.5f);
		if (DotProduct < 0.0f)
		{
			GetCharacterMovement()->MaxWalkSpeed = RunSpeedNormal;
		}
	}
}

void AMyTDGGameCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AMyTDGGameCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;

			MovementState = EMovementState::SprintRun_State;
		}
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}

	CharacterUpdate();

	//Weapon State Update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault* AMyTDGGameCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void AMyTDGGameCharacter::InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	UMyTDGGameInstance* myGI = Cast<UMyTDGGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(
					GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					

					myWeapon->WeaponSetting = myWeaponInfo;
					/*myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;*/
				
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;

					/*if (InventoryComponent)*/
					/*	CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);*/

					CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AMyTDGGameCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AMyTDGGameCharacter::WeaponReloadEnd);

					myWeapon->OnWeaponFireStart.AddDynamic(this, &AMyTDGGameCharacter::WeaponFireStart);

					//after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <=0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if(InventoryComponent)
					InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("AMyTDGGameCharacter::InitWeapon - Weapon not found in table -NULL"));
			}
		}
	}
}


void AMyTDGGameCharacter::TryReloadWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading)//fix reload
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
	}
}

void AMyTDGGameCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void AMyTDGGameCharacter::WeaponReloadEnd(bool bIsSuccess,int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSLotChangeValue(CurrentWeapon->WeaponSetting.WeaponType,AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
		
	}
	WeaponReloadEnd_BP(bIsSuccess);
}


void AMyTDGGameCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//BP
}

void AMyTDGGameCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//_BP
}


void AMyTDGGameCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP(Anim);
}





void AMyTDGGameCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

UDecalComponent* AMyTDGGameCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void AMyTDGGameCharacter::TrySwicthNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{

			}
		}
	}
}
	


void AMyTDGGameCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{

			}
		}
	}
}


void AMyTDGGameCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{

		UStateEffect* NewEffect = NewObject<UStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}
	}
}

EPhysicalSurface AMyTDGGameCharacter::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}


TArray<UStateEffect*> AMyTDGGameCharacter::GetAllCurrentEffects()
{
	return Effects;
}


void AMyTDGGameCharacter::RemoveEffect(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}


void AMyTDGGameCharacter::AddEffect(UStateEffect* newEffect)
{
	Effects.Add(newEffect);
}

void AMyTDGGameCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}
	
	bIsAlive = false;
	UnPossessed();

	//Timer ragdoll;
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer,this,&AMyTDGGameCharacter::EnableRagDoll,TimeAnim,false);
	GetCursorToWorld()->SetVisibility(false);
}


void AMyTDGGameCharacter::EnableRagDoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}



float AMyTDGGameCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfuceType());
		}
	}

	return ActualDamage;
}
