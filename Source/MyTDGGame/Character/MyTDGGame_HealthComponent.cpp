// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDGGame/Character/MyTDGGame_HealthComponent.h"

// Sets default values for this component's properties
UMyTDGGame_HealthComponent::UMyTDGGame_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMyTDGGame_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UMyTDGGame_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UMyTDGGame_HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UMyTDGGame_HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UMyTDGGame_HealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;

	Health += ChangeValue;

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			OnDead.Broadcast();
		}
	}

	OnHealthChange.Broadcast(Health, ChangeValue);
}

