// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDGGameGameMode.h"
#include "MyTDGGamePlayerController.h"
#include "MyTDGGame/Character/MyTDGGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyTDGGameGameMode::AMyTDGGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyTDGGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn>
		PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
