// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyTDGGameGameMode.generated.h"

UCLASS(minimalapi)
class AMyTDGGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyTDGGameGameMode();
};
