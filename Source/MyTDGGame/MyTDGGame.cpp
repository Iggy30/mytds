// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDGGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, MyTDGGame, "MyTDGGame");

DEFINE_LOG_CATEGORY(LogMyTDGGame)
